#-*- coding:utf-8 -*-
from random import Random
from time import time
from math import cos
from math import pi
import math
import numpy as np
import matplotlib.pyplot as plt

def CaclPlotRowColNum(n):
    if n == 1:
        #1*1
        return 111
    elif n == 2:
        #2*1
        return 211
    elif n == 3:
        #3*1
        return 311
    elif n == 4:
        #2*2
        return 221
    elif n == 5:
        #3*2
        return 321
    elif n == 6:
        #3*2
        return 321
    elif n == 7:
        #4*2
        return 241
    elif n == 8:
        #4*2
        return 421
    elif n == 9:
        #3*3
        return 331
    else:
        #3*3
        return 331

def Plot(names, datas, bPlotInOne):
    if len(names) < 2 or len(datas) < 2 or len(names) != len(datas):
        return

    xname = names[0]
    xdata = datas[0]

    plotNum = CaclPlotRowColNum(len(names)-1)

    for yname, ydata in zip(names, datas):
        if yname == xname:
            continue

        if not bPlotInOne:
            plt.subplot(plotNum)
            # plt.figure(plotNum)
            plotNum = plotNum + 1

        plt.plot(xdata, ydata, label=yname)
        plt.xlabel(xname)

        if not bPlotInOne:
            plt.ylabel(yname)
        #else:
            #n = len(xdata)/2
            #plt.text(xdata[n], ydata[n], yname)

        #plt.title('G-B Figure')
        #plt.axes(40,160,0,0.3)
        plt.grid(True)

def GetBestXgDatas(stat_file_name):
    gen = []
    best = []
    worst =[]
    average = []
    stdev =[]
    f = open(stat_file_name, 'r')
    for line in f:
        data = line.split(',')
        gen.append(int(data[0]))

        a,b,c,d=float(data[2]), float(data[3]), float(data[5]), float(data[6])
        #a=a+1
        #b=b+1
        #c=c+1
        #d=d+1
        #a = math.log10(a)
        #b = math.log10(b)
        #c = math.log10(c)
        #d = math.log10(d)
        worst.append(a)
        best.append(b)
        average.append(c)
        stdev.append(d)
    f.close()
    return gen, best,worst, average, stdev

def plotGA(statfile, bPlotInOne=False):
    #绘制4个曲线图
    #X轴--进化代数(Generation)
    #Y轴分别是:种群的最优适应值(Best Fitness), 种群的最差适应值(Worst Fitness), 种群的平均适应值(Average Fitness), 种群适应值的方差(Standard Deviation)
    names=['Generation', 'Best Fitness', 'Worst Fitness', 'Average Fitness', 'Standard Deviation']
    #从stat.csv中提取数据
    gen, best, worst, average,stdev=GetBestXgDatas(statfile)
    datas=[gen, best, worst, average, stdev]
    #绘制4个曲线图
    #Plot()函数的第3个参数表示是否将4个图画在一个图表里(1个figure有4个子图)
    #如果单独显示,可以将True改成False
    Plot(names, datas, bPlotInOne)

    #plt.figure(2)
    #Plot(names, datas, True)
    #plt.legend(loc='upper left' )
    # plt.show()
    plt.savefig('result_ga.png', dpi=100)
