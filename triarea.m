function a = triarea(b,h)
disp(sprintf('\t-->call function %s()', 'triarea'));
a = repmat(0.5*(b.* h), [3 3]);
% 必须将矩阵元素都转换为浮点数,否则python会报int64的错误!!!
a = double(a); % Convert array a to double