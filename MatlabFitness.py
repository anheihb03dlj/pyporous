#-*- coding:utf-8 -*-

import numpy as np
import matlab.engine

class MatlabFitness:
  def __init__(self, dim, Points, P0, DistFunc):
    print '(1)启动matlab engine....'
    self.eng = matlab.engine.start_matlab()
    self.dim = dim
    self.Points = Points
    self.P0 = P0
    self.DistFunc = DistFunc
    # 选出用于计算适应值的网格点
    m,n = self.dim
    self.T = [m*(j-1)+i for i,j in self.Points]
    self._P0 = [self.P0[t-1] for t in self.T]

  def __del__(self):
    print '(3)退出matlab engine....'
    self.eng.quit()
  
  def selectPoints(self, P):
    return [P[t-1] for t in self.T]

  def lbm(self, X):
    # 分解成m*n的矩阵
    A = np.array(X)
    A = A.reshape(self.dim)
    BOUND = matlab.double(A.tolist())
    # matlab的reshape始终是以列向量为标准
    # python的reshape则是以行向量为标准
    # BOUND.reshape(self.dim)

    # 调用matlab函数(porous.m)
    print '(2)调用matlab engine'
    ret = self.eng.porous2(BOUND, nargout=1)
    # 分解成一维数组 --> [[1,2,3,4...]]
    # print ret.size[0]*ret.size[1]
    ret.reshape((1, ret.size[0]*ret.size[1]))
    # print type(ret), ret
    P = [x for x in ret[0]] # matlab array转换为python列表
    # 返回LBM计算出来的一维数组(所有压力数据)
    return P

  def doIt(self, X):
    # 调用lbm程序进行模拟计算
    P = self.lbm(X)
    # 选出用于计算的网格点的压力数据!
    _P = self.selectPoints(P)
    # 比较P和P0,并计算两者之间的距离
    return self.DistFunc(_P, self._P0) # 相对误差最大值
    
