#-*- coding:utf-8 -*-

import numpy as np
import matlab.engine

def MakeResult(grids_file, pressures_file):
    print '(1)����matlab engine....'
    eng = matlab.engine.start_matlab()
    eng.make_result(grids_file, pressures_file, nargout=0)
    print '(3)�˳�matlab engine....'
    eng.quit()

if __name__ == '__main__':
    MakeResult('grids.txt', 'pressures.txt')