# pyporous

## 1、安装matlab engine(python调用Matlab, 需要Matlab 2014b以上版本)
* MatlabTest.py  --  测试python调用matlab是否成功！(在文件开头的注释中有安装方法链接！)
* 安装anaconda python（包含numpy和scipy）
* 安装模块: pip install inspyred

## 2、准备数据文件
* grids.txt  -- 网格(0-孔洞；1-实心)
* points.txt -- lbm程序模拟计算后, 哪些位置的数据需要输出, 每一行记录一个点所在的行和列(目前是网格中所有的100个点都输出！)
* pressures.txt -- 参见“MatlabPorous.m”的说明！遗传算法GA程序(test.py)读取该文件, 用于适应值的计算！

## 3、lbm程序(matlab版本)
* porous.m -- lbm程序(这个程序目前写的太死, 只能用于10*10网格的计算, 应将它改写, 并使之更通用化！)
            输入的参数：BOUND, 代表一个网格(由调用该函数的传入该数据，并不固定是10*10网格!!!)
            输出的结果：UA , 代表points.txt中每个点的压力值(目前是100个点都输出！UA实际也不是压力, 现在只是让程序能走通而已！)
* MatlabPorous.m -- 读取grids.txt网格, 生成一个BOUND网格数据(矩阵), 调用porous.m进行模拟, 将压力值写入到pressures.txt文件中！
                  如果grids.txt和points.txt文件没有被修改, 该代码仅需要被调用1次即可！

## 4、配置文件
* config.json  -- 记录了程序运行需要的一些配置参数(使用配置文件可以避免频繁修改代码！)
                其中遗传算法的参数可能是我们经常需要修改的(前4个)
                  
## 4、GA程序(python代码)
* MatlabFitness.py --  python调用Matlab函数(porous.m)进行适应值的计算
* MatlabPorous.py  --  python调用Matlab函数(MatlabPorous.m), 生成pressures.txt文件
* GAPorous.py  -- GA主程序, 该程序读取config.json配置文件,读取数据文件, 执行遗传算法计算过程