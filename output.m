function output( X, Y, filename )

[r,c]=size(X);
fid=fopen(filename,'w');
fprintf(fid, '遗传算法最优个体的压力数据:\r\n');
for i=1:r
    for j=1:c
        fprintf(fid,'%.5e\t',X(i,j));
    end
    fprintf(fid,'\r\n');
end

fprintf(fid,'\r\n原始的压力数据:\r\n');
for i=1:r
    for j=1:c
        fprintf(fid,'%.5e\t',Y(i,j));
    end
    fprintf(fid,'\r\n');
end

fclose(fid);

end

