#-*- coding:utf-8 -*-

'''
python调用matlab测试代码

参考资料:
    在Python中调用MATLAB的API(包括安装方法!!!)
    http://www.cnblogs.com/McKean/p/6390370.html

    Python调用Matlab2014b引擎(包括安装方法)
    http://blog.csdn.net/stereohomology/article/details/46277199

    MATLAB Engine API for Python(官方的帮助文档)
    http://cn.mathworks.com/help/matlab/matlab-engine-for-python.html

    解决 "array of MATLAB int64 cannot be returned on this platform错误"
    https://cn.mathworks.com/matlabcentral/answers/314191-problems-with-code-call-from-python

'''

import matlab.engine
import time

def basic_test(eng):
    print "Basic Testing Begin"
    print "eng.power(100,2) = %d"%eng.power(100,2)
    print "eng.max(100,200) = %d"%eng.max(100,200)
    print "eng.rand(5,5) = "
    print eng.rand(5,5)
    print "eng.randi(matlab.double([1,100]),matlab.double([3,4]))" % eng.randi(matlab.double([1,100]),matlab.double([3,4]))  
    print "Basic Testing Begin"  
  
def plot_test(eng):
    print "Plot Testing Begin"
    eng.workspace['data'] = eng.randi(matlab.double([1,100]),matlab.double([30,2]))
    eng.eval("plot(data(:,1),'ro-')")
    eng.hold('on',nargout=0)
    eng.eval("plot(data(:,2),'bx--')")
    print "Plot testing end"      
  
def audio_test(eng,freq,length):
    print "Audio Testing Begin"
    eval_str = "f = %d;t=%d;"%(freq,length)
    eng.eval(eval_str,nargout = 0)
    eng.eval('fs = 44100;T=1/fs;t=(0:T:t);',nargout = 0)
    eng.eval('y = sin(2 * pi * f * t);',nargout = 0)
    eng.eval('sound(y,fs);',nargout = 0)
    time.sleep(length)
    print "Audio Testing End"

def demo(eng):
    for i in range(10):
        # 调用triagrea.m中的函数
        ret = eng.triarea(i,5.0)
        print type(ret), ret

print '(1)启动matlab engine....'
eng = matlab.engine.start_matlab()

import numpy as np
print '(2)调用matlab engine'
A = [[1,2],[3,4],[5,6]]
B = matlab.double(A)
print type(B),B[0]
print eng.zeros(1,5)
demo(eng)

print '(3)退出matlab engine....'
eng.quit()