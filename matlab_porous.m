function matlab_porous(grids_file, pressures_file)
sprintf('\t-->read grid file:%s', grids_file);
BOUND = load(grids_file);
% BOUND = [0 1 0 1 0 1 0 1 0 0;0 0 0 0 1 0 0 1 0 0;0 0 1 0 0 1 0 1 0 1;0 1 1 1 0 1 0 1 0 1;0 0 0 1 1 1 0 0 0 1;0 1 0 1 0 1 0 1 0 0;0 0 0 0 1 0 0 1 0 0;0 0 1 0 0 1 0 1 0 1;0 1 1 1 0 1 0 1 0 1;0 0 0 1 1 1 0 0 0 1];
disp(BOUND)
pressures = porous2(BOUND);
disp(pressures)
% size(pressures)
sprintf('\t-->write pressures file:%s', pressures_file);
fid = fopen(pressures_file,'wt');
fprintf(fid,'%g\n',pressures);
fclose(fid);