#-*- coding:utf-8 -*-

import numpy as np
import matlab.engine

def MatlabPorous(grids_file, pressures_file):
    print '(1)����matlab engine....'
    eng = matlab.engine.start_matlab()
    eng.matlab_porous(grids_file, pressures_file, nargout=0)
    print '(3)�˳�matlab engine....'
    eng.quit()

if __name__ == '__main__':
    MatlabPorous('grids_30.txt', 'pressures_30.txt')