#-*- coding:utf-8 -*-
import math
import logging
import json
from random import Random
from time import time

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import distance

from inspyred import ec
from inspyred.ec import terminators
from inspyred.ec import observers
from inspyred.ec import analysis

import matlab.engine

from ga_plot import *
from MatlabFitness import *
from MatlabPorous import *
from MakeResult import *
from DistFunc import *

#约束处理,使0~1浮点数换成整数
# f2i = lambda a: [int(round(x)) for x in a]

# 自定义logger(暂时没有用上!)
def getLogger():
    logger = logging.getLogger('inspyred.ec')
    logger.setLevel(logging.DEBUG)
    file_handler = logging.FileHandler('inspyred.log', mode='w')
    file_handler.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
    return logger

#染色体随机生成器
#下面es.envovle()函数中的所有参数构成了一个词典args
def my_generator(random, args):
    # 提取附加的参数(维度:行数和列数)
    m, n = args['dim']
    #生成m*n个0~1范围的数
    # return [random.random() for i in range(m*n)]
    #生成m*n个0-1
    return [random.choice([0,1]) for i in range(m*n)]

# 计算适应值
# 注:真实的适应值计算是调用外部的适应值计算对象来完成的!!!
def callFitness(X, args):
    # 提取附加的参数(适应值函数计算器)
    # my_fitness = args['my_fitness']
    return args['my_fitness'].doIt(X)

#种群适应值函数
#pops是一个数组,它由多个染色体组成
def my_evaluator(candidates, args):
    #依次计算每个个体的适应值
    fitness = []
    for X in candidates:
        # 将浮点数四舍五入,强制转换为0和1,并计算个体的适应值
        score = callFitness(X, args)
        #添加到列表
        fitness.append(score)
    return fitness

# 自定义遗传算法终止条件
def my_terminator(population, num_generations, num_evaluations, args):
    if num_generations > args['max_generations']:
      return True
    min_fitness = args.get('minimum_fitness', 0.01)
    pop_fitness = my_evaluator([x.candidate for x in population], args)
    print pop_fitness
    return max(pop_fitness) < min_fitness

#inspyred也提供了多种终止条件
#例如evaluation_termination--表示适应值计算多少次后就就终止,无论是否达到最优,也不考虑算法是否收敛
#    generation_termination--表示进化多少代之后就终止,无论是否达到最优,也不考虑算法是否收敛
#    average_fitness_termination --表示种群的平均适应值连续多少代没有显著变化就终止,因为进化过程已经收敛或者假收敛了
#如果选择的是evaluation_termination,那么要在下面的envovle函数中设置max_evaluations参数
#es.terminator = terminators.evaluation_termination
#如果选择的是evaluation_termination,那么要在下面的envovle函数中设置max_generations参数
# es.terminator = terminators.generation_termination
#如果选择的是average_fitness_termination,无需设置额外的终止条件参数
#有可能会收敛的比较慢(说不定要算上几个小时),这时可以考虑选择其它的2个终止条件
# es.terminator = terminators.average_fitness_termination
# es.terminator = my_terminator
TerminatorDict = {
    # 按最大进化代数终止
    1:terminators.generation_termination,
    # 按最大适应值计算次数终止
    2:terminators.evaluation_termination,
    # 种群的平均适应值趋于平稳不变时终止
    3:terminators.average_fitness_termination,
    # 自定义终止条件(按最大相对误差达到一定精度后终止)
    4:my_terminator
}

# 从词典中选择终止条件函数
def selectTerminator(terminator_type):
    if not (terminator_type in TerminatorDict):
        terminator_type = 1
    return TerminatorDict[terminator_type]

# 遗传算法主框架
def runGA(args):
    #随机数初始化
    rand = Random()
    #如果设置的seed是一个固定值,那么优化算法的结果始终是固定不变的!!!
    #利用当前时间作为随机数种子,就可以看到不同的优化结果!!!
    rand.seed(int(time()))

    #选择优化算法
    #inspyred提供了多种智能优化算法
    #例如GA(遗传算法)、NSGA2(好像是多目标优化算法)、DEA(微分进化算法)、SA(模拟退火算法)、PSO(粒子群算法)等等
    es = ec.GA(rand)

    #设置遗传算法终止条件
    es.terminator = selectTerminator(args['terminator'])

    #设置数据观察者
    #file_observer是inspyred提供了一个数据接口,它会收集进化过程中的数据,并写入到文件
    #我们可以通过这些文件来生成进化曲线,观察进化过程和优化结果是否满足我们的需要
    es.observer=observers.file_observer
    #es.observer=observers.plot_observer

    stat_file=open(args['stat_file'], 'w')
    ind_file=open(args['ind_file'],'w')

    #设置其它参数并开始进化
    #返回的是最后一次进化的种群
    #如果优化过程真正的收敛了,final_pop里的最优个体就是我们要找的最优解
    final_pop = es.evolve(generator = my_generator,   #染色体生成器(indi generator),envovle函数内部调用它连续生成多个个体,组成初始种群
                        evaluator = my_evaluator,   #适应值函数,计算个体的适应值,评价个体的好坏
                        pop_size = args['popsize'], #种群规模(种群内个体的个数)
                        maximize = False,            #求最大优化问题还是最小优化问题
                        bounder = ec.Bounder(0, 1), #个体的上下限(每个分量的范围都限制在0~1内)
                        # max_evaluations = 1200,   #如果前面设置的是: es.terminator = terminators.evaluation_termination 需要设置该参数
                        max_generations = args['max_generations'],   #如果前面设置的是: es.terminator = terminators.generation_termination 需要设置该参数
                        mutation_rate = args['mutation_rate'],   #变异率
                        cross_rate = args['cross_rate'],         #交叉率
                        # num_elites = 1,
                        statistics_file = stat_file,   #设置进化统计数据要写入的文件(es.observer=observers.file_observer专用)
                        individuals_file = ind_file,   #设置进化统计数据要写入的文件(es.observer=observers.file_observer专用)
                        dim = args['dim'],                   # 附加参数--网格的维度(行和列)
                        my_fitness = args['my_fitness'],     # 附加参数--适应值计算器对象
                        minimum_fitness = args['minimum_fitness'])    # 附加参数--最小适应值
    #进化完成后关闭文件
    stat_file.close()
    ind_file.close()

    #analysis.generation_plot('stat.csv')
    #analysis.allele_plot('ind.csv')

    #对种群进行逆序排序(个体按照适应值从大到小排序,适应值越大,我们认为该个体最优)
    final_pop.sort(reverse=True)
    # 打印最优解(排序后的种群中第1个个体)
    return final_pop[0].candidate

# 从文件中读取网格信息(网格的行数和列数)
def readGridInfo(filename):
    A = np.loadtxt(filename)
    return A.shape

# 从文件中读取要输出的位置信息(点所在的行和列)
def readPointInfo(filename):
    # 以整数的方式读取文件
    A = np.loadtxt(filename,dtype='int32')
    # A = A - 1 # python中索引值从0开始,文件中记录的索引值从1开始
    return A.tolist()

# 读取模拟的真实结果(若干点的压力数据)
def readPointValues(filename):
    A = np.loadtxt(filename)
    return A

# 读取json格式的配置文件
def readConfig(filename):
    # 读取json配置文件 
    f = open(filename, 'r')
    root = json.load(f)
    f.close()
    return root

# Python2 JSON.load成Unicode的坑
# https://segmentfault.com/a/1190000002779638
def byteify(input):
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def askNewResult():
    ans = raw_input('\n是否重新生成压力数据文件[Y/N]?:')
    ans = ans.lower()
    ret = False
    if ans == 'y' or ans == 'yes':
        ret = True
    return ret

def writeBestIndi(best_indi, paramConfig):
    # 转换为0-1编码
    X = best_indi
    # 写入到new_grids.txt文件
    A = np.array(X)
    A = A.reshape(paramConfig['dim'])
    np.savetxt('best_'+paramConfig['grids_file'], A, fmt='%d', delimiter='\t')
    # 调用matlab计算
    P = np.array(paramConfig['my_fitness'].lbm(X))
    # 写入到文件
    np.savetxt('best_'+paramConfig['pressures_file'], P, fmt='%.5e', delimiter='\t')

def main():
    start = time()
    # 读取json格式配置文件
    paramConfig = readConfig('config.json')
    # 将unicode字符串变成str
    paramConfig = byteify(paramConfig)

    # 读取文件,获取网格的行数和列数
    paramConfig['dim'] = readGridInfo(paramConfig['grids_file'])
    # 重新生成压力数据(如果grids.txt和points.txt没有修改,仅需要运行一次即可!!!)
    if askNewResult() == True:
        MatlabPorous(paramConfig['grids_file'], paramConfig['pressures_file'])
    # 读取文件,获取位置信息
    paramConfig['points'] = readPointInfo(paramConfig['points_file'])
    # 读取真实模拟结果(若干点的压力)
    paramConfig['pressures'] = readPointValues(paramConfig['pressures_file'])
    # 获取适应值距离计算函数句柄
    DistFunc = getDistFunc(paramConfig['dist_func'])
    # 构造matlab的计算器
    paramConfig['my_fitness'] = MatlabFitness(paramConfig['dim'], paramConfig['points'], paramConfig['pressures'], DistFunc)
    # print paramConfig

    print '-------------开始GA计算---------------------'
    best_indi = runGA(paramConfig)

    print '最优个体:', best_indi
    print '最优适应值:', callFitness(best_indi, paramConfig)
    print '-------------结束GA计算---------------------'
    # 写入到文件
    writeBestIndi(best_indi, paramConfig)

    # 生成网格图片以及压力数据对比文件
    MakeResult(paramConfig['grids_file'], paramConfig['pressures_file'])

    # 绘制图形
    plotGA(paramConfig['stat_file'], False)

    stop = time()
    print '总计算耗时:', stop-start

    # 写入一些统计信息,记录到文件中
    f = open('统计信息.txt', 'w')
    f.write('总计算耗时 -- %d (s)\n' % int(stop-start))
    f.write('遗传算法参数\n')
    f.write('\t种群大小 -- %d\n' % paramConfig['popsize'])  
    f.write('\t交叉率 -- %.2f\n' % paramConfig['cross_rate'])
    f.write('\t变异率 -- %.2f\n' % paramConfig['mutation_rate'])
    f.write('\t适应值计算函数 -- %s\n' % getDistFuncName(paramConfig['dist_func']))
    f.write('\t终止条件 -- ')
    if paramConfig['terminator'] == 1:
        f.write(' 最大进化代数:%d\n' % paramConfig['max_generations'])
    elif paramConfig['terminator'] == 2:
        f.write(' 最大适应值计算次数:%d\n' % paramConfig['max_evaluations'])
    elif paramConfig['terminator'] == 3:
        f.write(' 种群平均值自适应收敛(无需设置额外的参数)\n')
    elif paramConfig['terminator'] == 4:
        f.write(' 精度:%f' % paramConfig['minimum_fitness'])
        f.write(' 最大进化代数:%d\n' % paramConfig['max_generations'])
    f.write('\t最优个体 -- %s\n' % str(best_indi))
    f.write('\t最优适应值 -- %f\n' % callFitness(best_indi, paramConfig))
    f.close()

    # 销毁matlab engine
    del paramConfig['my_fitness']

if __name__ == '__main__':
    main() # 主函数入口

    # if __name__ == "__main__":
    #   P = [9.30539e-08,4.75333e-08,3.32764e-08,1.84094e-08,0.001]
    #   P0 = [9.10539e-08,3.75333e-08,3.324e-07,1.84094e-08,0]
    #   print max_relative_tolerance(P, P0)