#-*- coding:utf-8 -*-
import math
import numpy as np
from scipy.spatial import distance

# 计算欧氏距离(类似于两点之间的距离、最小二乘、方差的概念)
# 使用scipy提供的距离度量函数
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.cdist.html#scipy.spatial.distance.cdist
def euclidean_distance(P, P0):
    print '使用欧氏距离进行计算....'
    dist = distance.cdist([P],[P0],'euclidean') # 欧氏距离
    return dist[0, 0]*1e2
    # return np.log10(dist[0, 0])+10

# 计算相对误差,并取最大
# 由于适应值的数量级较大,采用对数函数进行处理
def max_relative_tolerance(P, P0):
    print '使用最大相对误差进行计算....'
    P = np.array(P)*1e5
    P0 = np.array(P0)*1e5
    delta = 1e-5
    X = np.abs(P-P0)/(np.abs(P0)+delta)
    # print X
    # print np.log(X+1)
    # print np.log10(X+1)
    return np.log(max(X)+1)

# 计算相对误差,并取最大
# 由于适应值的数量级较大,采用对数函数进行处理
def relative_tolerance_1_norm(P, P0):
    print '使用相对误差的绝对值求和....'
    P = np.array(P)*1e5
    P0 = np.array(P0)*1e5
    delta = 1e-5
    X = np.abs(P-P0)/(np.abs(P0)+delta)
    return np.log(np.sum(X)+1)

def _f(p, p0):
    if abs(p) < 1e-3:
        if abs(p0) < 1e-3:
            return 1e6  # p=0, p0=0
        else:
            return 0.0  # p=0, p0!=0
    else:
        if abs(p0) < 1e-3:
            return 0.0  # p!=0, p0=0
        elif abs(p-p0) < 1e-3:
            return 1e6  # p=p0, p!=0, p0!=0
        else:
            return 1.0*abs(p0)/abs(p-p0)   # p!=p0, p!=0, p0!=0

def _g(p, p0):
    if abs(p) < 1e-3:
        if abs(p0) < 1e-3:
            return 0.0  # p=0, p0=0
        else:
            return 1e6  # p=0, p0!=0
    else:
        if abs(p0) < 1e-3:
            return 1e6  # p!=0, p0=0
        elif abs(p-p0) < 1e-3:
            return 0  # p=p0, p!=0, p0!=0
        else:
            return 1.0*abs(p-p0)/abs(p0)   # p!=p0, p!=0, p0!=0

def grid_fitness(P, P0):
    print '使用格倒数适应值进行计算....'
    P = np.array(P)*1e5
    P0 = np.array(P0)*1e5
    X = [_g(p, p0) for p, p0 in zip(P, P0)]
    print X
    return np.sum(X)/len(X)

DistFuncDict = {
    1:("欧氏距离", euclidean_distance),
    2:("最大相对误差", max_relative_tolerance),
    3:("相对误差1范数(绝对值求和)", relative_tolerance_1_norm),
    4:("格倒数", grid_fitness)
}

def getDistFunc(dist_type):
    if not (dist_type in DistFuncDict):
        dist_type = 0
    return DistFuncDict[dist_type][1]

def getDistFuncName(dist_type):
    if not (dist_type in DistFuncDict):
        dist_type = 0
    return DistFuncDict[dist_type][0]