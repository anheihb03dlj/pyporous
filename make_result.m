function make_result(grids_file, pressures_file)
% 加载数据
BEST_GRIDS=load(strcat('best_', grids_file));
GRIDS=load(grids_file);
% 绘制网格
fig=figure(1);
set(fig,'visible','off');
% set(gcf,'Position',get(0,'ScreenSize'));
subplot(1,2,1);
colormap(gray(2));
image(2-BEST_GRIDS);
title('遗传算法生成的网格(参数:种群-100, 进化代数-100')
% hold on;
subplot(1,2,2);
colormap(gray(2));
image(2-GRIDS);
title('原始网格');
% set(gcf,'Position',[500,500,500,500], 'color','w')
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 14 5]);
print -dpng result_grids.png -r100
% saveas(gcf,'result_grids.png');
saveas(gcf, 'result_grids.fig'); 

[m,n] = size(GRIDS);
BEST_P = load(strcat('best_',pressures_file));
P = load(pressures_file);
BEST_P = reshape(BEST_P, n, m); % matlab是按列向量输出的P，但网格是按行向量格式编制的
P = reshape(P, n, m); % matlab是按列向量输出的P，但网格是按行向量格式编制的
% BEST_P = BEST_P';
% P = P';

% 输出到文件
output(BEST_P, P, strcat('result_',pressures_file));
